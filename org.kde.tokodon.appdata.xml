<?xml version="1.0" encoding="utf-8"?>
<!--
  - SPDX-License-Identifier: CC0-1.0
  - SPDX-FileCopyrightText: 2020-2021 Carl Schwan <carlschwan@kde.org>
 -->
<component type="desktop">
  <id>org.kde.tokodon</id>
  <provides>
    <binary>tokodon</binary>
  </provides>
  <name>Tokodon</name>
  <name xml:lang="ca">Tokodon</name>
  <name xml:lang="ca-valencia">Tokodon</name>
  <name xml:lang="cs">Tokodon</name>
  <name xml:lang="de">Tokodon</name>
  <name xml:lang="en-GB">Tokodon</name>
  <name xml:lang="es">Tokodon</name>
  <name xml:lang="it">Tokodon</name>
  <name xml:lang="ko">Tokodon</name>
  <name xml:lang="nl">Tokodon</name>
  <name xml:lang="pl">Tokodon</name>
  <name xml:lang="pt">Tokodon</name>
  <name xml:lang="sk">Tokodon</name>
  <name xml:lang="sl">Tokodon</name>
  <name xml:lang="sv">Tokodon</name>
  <name xml:lang="uk">Tokodon</name>
  <name xml:lang="x-test">xxTokodonxx</name>
  <summary>A native Mastodon client</summary>
  <summary xml:lang="ca">Un client natiu del Mastodon</summary>
  <summary xml:lang="ca-valencia">Un client natiu del Mastodon</summary>
  <summary xml:lang="de">Ein Mastodon-Client</summary>
  <summary xml:lang="en-GB">A native Mastodon client</summary>
  <summary xml:lang="es">Un cliente nativo de Mastodon</summary>
  <summary xml:lang="it">Un client nativo Mastodon</summary>
  <summary xml:lang="ko">네이티브 마스토돈 클라이언트</summary>
  <summary xml:lang="nl">Een inheemse Mastodon-client</summary>
  <summary xml:lang="pl">Program do Mastodona</summary>
  <summary xml:lang="pt">Um cliente nativo do Mastodon</summary>
  <summary xml:lang="sk">Natívny klient pre Mastodon</summary>
  <summary xml:lang="sl">Domorodni odjemalec Mastodon</summary>
  <summary xml:lang="sv">En inbyggd Mastodon-klient</summary>
  <summary xml:lang="uk">Власний клієнт Mastodon</summary>
  <summary xml:lang="x-test">xxA native Mastodon clientxx</summary>
  <description>
    <p>Tokodon is a Mastodon client. It allows you to interact with the Fediverse community.</p>
    <p xml:lang="ca">El Tokodon és un client Mastodon. Permet que interactueu amb la comunitat Fediverse.</p>
    <p xml:lang="ca-valencia">El Tokodon és un client Mastodon. Permet que interactueu amb la comunitat Fediverse.</p>
    <p xml:lang="de">Tokodon ist ein Mastodon-Client. Er ermöglicht Ihnen die Interaktion mit der Fediverse-Gemeinschaft.</p>
    <p xml:lang="en-GB">Tokodon is a Mastodon client. It allows you to interact with the Fediverse community.</p>
    <p xml:lang="es">Tokodon es un cliente de Mastodon. Le permite interactuar con la comunidad del Fediverso.</p>
    <p xml:lang="it">Tokodon è un cliente Mastodon. Ti consente di interagire con la comunità di Fediverse.</p>
    <p xml:lang="ko">Tokodon은 마스토돈 클라이언트입니다. Fediverse 커뮤니티를 사용할 수 있습니다.</p>
    <p xml:lang="nl">Tokodon is een Mastodon-client. Het biedt u interactie met de Fediverse gemeenschap.</p>
    <p xml:lang="pl">Tokodon to program do Mastodona. Umożliwia działanie w społeczności Fediverse.</p>
    <p xml:lang="pt">O Tokodon é um cliente de Mastodon. Permite-lhe interagir com a comunidade Fediverse.</p>
    <p xml:lang="sl">Tokodon je odjemalec za Mastodon. Omogoča interakcijo s skupnostjo Fediverse.</p>
    <p xml:lang="sv">Tokodon är en Mastodon-klient. Den låter dig interagera med Fediverse-gemenskapen.</p>
    <p xml:lang="uk">Tokodon — клієнт Mastodon. За його допомогою ви можете спілкуватися із учасниками спільноти Fediverse.</p>
    <p xml:lang="x-test">xxTokodon is a Mastodon client. It allows you to interact with the Fediverse community.xx</p>
  </description>
  <url type="homepage">https://apps.kde.org/tokodon/</url>
  <url type="bugtracker">https://invent.kde.org/network/tokodon/-/issues</url>
  <categories>
    <category>Network</category>
  </categories>
  <developer_name>The KDE Community</developer_name>
  <developer_name xml:lang="ca">La comunitat KDE</developer_name>
  <developer_name xml:lang="ca-valencia">La comunitat KDE</developer_name>
  <developer_name xml:lang="cs">Komunita KDE</developer_name>
  <developer_name xml:lang="de">Die KDE-Gemeinschaft</developer_name>
  <developer_name xml:lang="en-GB">The KDE Community</developer_name>
  <developer_name xml:lang="es">La Comunidad KDE</developer_name>
  <developer_name xml:lang="it">La comunità KDE</developer_name>
  <developer_name xml:lang="ko">KDE 커뮤니티</developer_name>
  <developer_name xml:lang="nl">De KDE gemeenschap</developer_name>
  <developer_name xml:lang="pl">Społeczność KDE</developer_name>
  <developer_name xml:lang="pt">A Comunidade do KDE</developer_name>
  <developer_name xml:lang="sk">KDE Komunita</developer_name>
  <developer_name xml:lang="sl">Skupnost KDE</developer_name>
  <developer_name xml:lang="sv">KDE-gemenskapen</developer_name>
  <developer_name xml:lang="uk">Спільнота KDE</developer_name>
  <developer_name xml:lang="x-test">xxThe KDE Communityxx</developer_name>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <value key="KDE::matrix">#tokodon:kde.org</value>
  <content_rating type="oars-1.1">
    <content_attribute id="social-chat">intense</content_attribute>
  </content_rating>
  <releases>
    <release version="21.08" date="2021-08-31">
      <url>https://www.plasma-mobile.org/2021/08/31/plasma-mobile-gear-21-08/</url>
    </release>
    <release version="21.07" date="2021-07-20">
      <url>https://www.plasma-mobile.org/2021/07/20/plasma-mobile-gear-21-07/</url>
    </release>
    <release version="21.06" date="2021-06-10">
      <description>
        <p>First beta release of Tokodon, with most basic feature working (timeline, sending posts, replying, adding attachements, viewing images and multi-account support).</p>
        <p>Currently big missing features are the support for polls, videos and notifications.</p>
      </description>
      <url>https://www.plasma-mobile.org/2021/06/10/plasma-mobile-update-june/</url>
    </release>
  </releases>
</component>
